package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trade;

/**
 * Interface which contains methods which will act upon database
 * @author Administrator
 *
 */
public interface TradeDao {
	
	List<Trade> findAll();

    Trade findById(int id);

    Trade create(Trade trade);

    void deleteById(int id);

}
