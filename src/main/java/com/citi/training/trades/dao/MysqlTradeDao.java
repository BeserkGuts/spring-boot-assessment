package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trade;

/**
 * Class which implements TradeDao. 
 * Runs queries on the database
 * @author Administrator
 *
 */
@Component
public class MysqlTradeDao implements TradeDao{
	
	@Autowired
    JdbcTemplate tpl;
	
	/**
	 * Finas all {@link com.citi.training.model.Trade}'s.
	 */
	public List<Trade> findAll() {
        return tpl.query("SELECT id, stock, price, volume FROM trade",
                         new TradeMapper());
    }

    /**
     * Delete an {@link com.citi.training.model.Trade} by integer id
     * Initially finds {@link com.citi.training.model.Trade} by id, then performs SQL DELETE query
     */
    public void deleteById(int id) {
        findById(id);
        tpl.update("DELETE FROM trade WHERE id=?", id);
    }

    /**
     * Finds an {@link com.citi.training.model.Trade} by integer id
     * Selects id, stock, price, volume from trade table. 
     * Throws {@link com.citi.training.trades.exceptions.TradeNotFoundException} exception if 
     * not {@link com.citi.training.model.Trade} returned 
     * 
     * Will return a single {@link com.citi.training.model.Trade}
     */
    public Trade findById(int id) {
        List<Trade> trades = tpl.query(
                "SELECT id, stock, price, volume FROM trade WHERE id=?",
                new Object[] {id},
                new TradeMapper()
        );
        
        if(trades.size() <= 0) {
            throw new TradeNotFoundException("Trade with id=[" + id +
                                                 "] not found");
        }
        
        return trades.get(0);
        
    }

    /**
     * Runs query to create an {@link com.citi.training.model.Trade}. 
     * Generates a keyholder to auto-generate keys to use as Trade id. 
     * 
     */
    public Trade create(Trade trade) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        tpl.update(
           new PreparedStatementCreator() {
               @Override
               public PreparedStatement createPreparedStatement(Connection connection)
                                                                   throws SQLException {

                   PreparedStatement ps =
                           connection.prepareStatement(
                                   "insert into trade (stock, price, volume) values (?, ?, ?)",
                           Statement.RETURN_GENERATED_KEYS);
                   ps.setString(1,  trade.getStock());
                   ps.setDouble(2,  trade.getPrice());
                   ps.setInt(3,  trade.getVolume());
                   return ps;

               }
           },
           keyHolder
         );
        trade.setId(keyHolder.getKey().intValue());
        return trade;
    }
    
    /**
     * Class which maps data into a row in the database, on a current per-row basis 
     * @author Administrator
     *
     */
    private static final class TradeMapper implements RowMapper<Trade>{

        public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Trade(rs.getInt("id"),
                                rs.getString("stock"),
                                rs.getDouble("price"),
                                rs.getInt("volume"));
        }

    }

}
