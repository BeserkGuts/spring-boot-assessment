package com.citi.training.trades.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.trades.dao.TradeDao;
import com.citi.training.trades.model.Trade;

/**
 * Class which implements tradeDao
 * Further abstracts controller from database, as per good architecture design 
 * @author Administrator
 *
 */
@Component
public class TradeService {
	
	@Autowired
	private TradeDao tradeDao;
	
	/**
	 * Returns all trades within database
	 * @return
	 */
	public List<Trade> findAll(){
		return tradeDao.findAll();
	}

	/**
	 * Returns a trade based on a given integer (id) vaue
	 * @param id
	 * @return
	 */
    public Trade findById(int id) {
    	return tradeDao.findById(id);
    }

    /**
     * Creates a trade given a trade object 
     * @param trade
     * @return
     */
    public Trade create(Trade trade) {
    	//if(trade.getStock().length() >0 && trade.getPrice()>0 && trade.getVolume() >0) {
    	if(trade.getStock().length() >0 ) {
    	return tradeDao.create(trade);
    	} else {
    		throw new RuntimeException("Invalid Parameter: trade name: "
    				+trade.getStock());
    	}
    }

    /**
     * Delete a trade through being passed an id
     * @param id
     */
    public void deleteById(int id) {
    	tradeDao.deleteById(id);
    }

}
