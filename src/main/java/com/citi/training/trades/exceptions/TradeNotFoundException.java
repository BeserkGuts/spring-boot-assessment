package com.citi.training.trades.exceptions;

/**
 * Custom exception class, for when {@link com.citi.training.model.Trade} is not found through id
 * @author Administrator
 *
 */
@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException{
	
	public TradeNotFoundException(String message) {
		super(message);
	}

}
