package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trade;
import com.citi.training.trades.services.TradeService;

@RestController
@RequestMapping("/trades")
public class TradeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);
	 
    @Autowired
    private TradeService tradeService;

    /**
     * Find all {@link com.citi.training.trades.model.Trade}'s. 
     * @return
     */
    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> findAll(){
        LOG.debug("findAll() was called");
        LOG.info("Trade findAll method called");
        return tradeService.findAll();
    }

    /**
     * Find an {@link com.citi.training.trades.model.Trade} by its integer id. 
     * @param id
     * @return
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Trade findById(@PathVariable int id) {
        LOG.debug("findById() was called, id: " + id);
        LOG.info("Trade findById was called");
        return tradeService.findById(id);
    }

    /**
     * Create an {@link com.citi.training.trades.model.Trade}, by taking in a Trade Object
     * Return Http Status CREATED
     * @param trade
     * @return
     */
    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> create(@RequestBody Trade trade) {
    	LOG.info("Trade create method called");
        LOG.debug("create was called, trade: " + trade);
        return new ResponseEntity<Trade>(tradeService.create(trade),
                                            HttpStatus.CREATED);
    }

    /**
     * Delete an {@link com.citi.training.trades.model.Trade} by their integer id. 
     * @param id
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById was called, id: " + id);
        LOG.info("Trade delete method was called");
        tradeService.deleteById(id);
    }

}
