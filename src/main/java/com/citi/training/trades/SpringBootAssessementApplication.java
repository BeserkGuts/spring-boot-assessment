package com.citi.training.trades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAssessementApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAssessementApplication.class, args);
	}

}
