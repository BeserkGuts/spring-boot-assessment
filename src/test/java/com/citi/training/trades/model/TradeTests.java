package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradeTests {
	
	private int testId =1;
	private String testStock = "ABCD";
	private double testPrice = 9.99;
	private int testVolume = 10;
	
	@Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testId, testStock, testPrice, testVolume);

        assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
        assertEquals(testPrice, testTrade.getPrice(), 0.0001);
        assertEquals(testVolume, testTrade.getVolume());
    }
	
	  @Test
	    public void test_Trade_toString() {
	        String testString = new Trade(testId, testStock, testPrice, testVolume).toString();

	        assertTrue(testString.contains((new Integer(testId)).toString()));
	        assertTrue(testString.contains(testStock));
	        assertTrue(testString.contains(String.valueOf(testPrice)));
	        assertTrue(testString.contains((new Integer(testVolume).toString())));
	    }

}
